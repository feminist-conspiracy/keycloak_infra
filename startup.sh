#/bin/bash

export DB_PASSWORD=$(echo $DB_SECRETS | jq -r .password)

# Note: External container connectivity is always provided by eth0 -- irrespective of which is considered public/private by KC.
#       In case the container needs to be accessible on the host computer override -b $PUBLIC_IP by adding: `-b 0.0.0.0` to the docker command.

if [ $KEYCLOAK_ADMIN_USER ] && [ $KEYCLOAK_ADMIN_PASSWORD ]; then
    $JBOSS_HOME/bin/add-user-keycloak.sh --user "${KEYCLOAK_ADMIN_USER}" --password "${KEYCLOAK_ADMIN_PASSWORD}"
fi

echo "DB_PASSWORD: " $DB_PASSWORD

unset KEYCLOAK_ADMIN_PASSWORD
unset KEYCLOAK_ADMIN_USER

exec /opt/jboss/tools/docker-entrypoint.sh -b 0.0.0.0
