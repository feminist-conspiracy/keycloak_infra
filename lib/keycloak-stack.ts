import * as cdk from "@aws-cdk/core";
import * as rds from "@aws-cdk/aws-rds";
import * as ec2 from "@aws-cdk/aws-ec2";
import * as ecsPattern from "@aws-cdk/aws-ecs-patterns";
import * as ecs from "@aws-cdk/aws-ecs";
import * as elb from "@aws-cdk/aws-elasticloadbalancingv2";
import * as cloudfront from "@aws-cdk/aws-cloudfront";
import * as targets from "@aws-cdk/aws-route53-targets";
import * as route53 from "@aws-cdk/aws-route53";
import * as acm from "@aws-cdk/aws-certificatemanager";
import * as secretsManager from "@aws-cdk/aws-secretsmanager";

export class KeycloakStack extends cdk.Stack {
  constructor(scope: cdk.Construct, id: string, props?: cdk.StackProps) {
    super(scope, id, props);

    const databaseUsername = "keycloak";
    const databaseName = "keycloak";
    const base = "ovarit.net";
    const subname = "auth.test";

    const vpc = new ec2.Vpc(this, "KeycloackVPC", {
      enableDnsHostnames: false,
    });

    // Set up RDS

    const database = new rds.DatabaseInstance(this, "db", {
      engine: rds.DatabaseInstanceEngine.POSTGRES,
      instanceType: ec2.InstanceType.of(
        ec2.InstanceClass.BURSTABLE3,
        ec2.InstanceSize.SMALL
      ), // TODO use nano for non "main" builds. Also, right size this.
      masterUsername: databaseUsername,
      vpc,
      storageEncrypted: true,
      multiAz: false,
      databaseName: databaseName,
      storageType: rds.StorageType.GP2,
      // TODO monitor performance and log things to cloudwatch
    });
    database.connections.allowFrom(
      ec2.Peer.ipv4(vpc.vpcCidrBlock),
      ec2.Port.tcp(5432)
    );

    const keycloakSecret = new secretsManager.Secret(this, "keycloak");

    const environment = {
      KEYCLOAK_USER: "admin",
      DB_VENDOR: "postgres",
      DB_USER: databaseUsername,
      DB_PORT: database.dbInstanceEndpointPort,
      DB_ADDR: database.dbInstanceEndpointAddress,
      KEYCLOAK_FRONTEND_URL: `https://${base}.${subname}/`,
      PROXY_ADDRESS_FORWARDING: "true",
    };
    const secrets = {
      KEYCLOAK_PASSWORD: ecs.Secret.fromSecretsManager(keycloakSecret),
      DB_SECRETS: ecs.Secret.fromSecretsManager(database.secret!),
    };

    // Set up KeyCloak

    const cluster = new ecs.Cluster(this, "throat-cluster", {
      vpc,
    });

    const ecsStack = new ecsPattern.ApplicationLoadBalancedFargateService(
      this,
      "fargate",
      {
        cluster,
        cpu: 512,
        memoryLimitMiB: 1024,
        publicLoadBalancer: true,
        protocol: elb.ApplicationProtocol.HTTP,
        healthCheckGracePeriod: cdk.Duration.minutes(5),
        taskImageOptions: {
          image: ecs.ContainerImage.fromAsset("./"),
          family: ec2.InstanceClass.BURSTABLE3,
          environment,
          secrets,
          containerPort: 8080,
        },
      }
    );

    const zone = route53.HostedZone.fromLookup(this, "primary-zone", {
      domainName: base,
    });

    const cfnDomainName = `${subname}.${base}`;

    const certArn = new acm.DnsValidatedCertificate(this, "cft-dist-cert", {
      domainName: cfnDomainName,
      hostedZone: zone,
      region: "us-east-1",
    }).certificateArn;

    const cfdistro = new cloudfront.CloudFrontWebDistribution(
      this,
      "distribution",
      {
        aliasConfiguration: {
          acmCertRef: certArn,
          names: [cfnDomainName],
          sslMethod: cloudfront.SSLMethod.SNI,
          securityPolicy: cloudfront.SecurityPolicyProtocol.TLS_V1_2_2018,
        },
        originConfigs: [
          {
            customOriginSource: {
              domainName: ecsStack.loadBalancer.loadBalancerDnsName,
              originProtocolPolicy: cloudfront.OriginProtocolPolicy.HTTP_ONLY,
            },
            behaviors: [
              {
                allowedMethods: cloudfront.CloudFrontAllowedMethods.ALL,
                compress: true,
                isDefaultBehavior: true,
                forwardedValues: {
                  queryString: true,
                  headers: ["Host"],
                  cookies: { forward: "all" },
                },
              },
            ],
          },
        ],
      }
    );

    new route53.ARecord(this, "cfn-ARecord", {
      zone,
      recordName: cfnDomainName,
      target: route53.RecordTarget.fromAlias(
        new targets.CloudFrontTarget(cfdistro)
      ),
    });
    new route53.AaaaRecord(this, "cfn-AaaaRecord", {
      zone,
      recordName: cfnDomainName,
      target: route53.RecordTarget.fromAlias(
        new targets.CloudFrontTarget(cfdistro)
      ),
    });
  }
}
