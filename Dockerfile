FROM jboss/keycloak

USER root

RUN curl -L -o /bin/jq "https://github.com/stedolan/jq/releases/download/jq-1.6/jq-linux64"
RUN chmod 755 /bin/jq
COPY startup.sh /startup.sh
RUN chmod 755 /startup.sh

USER 1000

ENTRYPOINT [ "/bin/bash" ]
CMD [ "/startup.sh" ]
